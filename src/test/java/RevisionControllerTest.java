import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.neo4j.issue.datanodes.Application;


@RunWith(SpringRunner.class)
@SpringBootTest(classes={Application.class})
@WebAppConfiguration
@AutoConfigureMockMvc
public class RevisionControllerTest {

	@Autowired
	private MockMvc mockMvc;

	
	@Test
	public void createRevisionTest() throws Exception{
		mockMvc.perform(post("/createRevision")
				.contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"revisionNo\":8}"))
		.andDo(MockMvcResultHandlers.print());
	}
}
