package com.neo4j.issue.datanodes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RevisionController {

	@Autowired
	RevisionService revisionService;
	
	/**
	 * sample request would be
	 * http://localhost:8080/createRevision POST
	 * {
		"revisionNo" : "88"
		}
	 * @param revisionDTO
	 * @return
	 */
	 @RequestMapping("/createRevision")
	    public String creatRevision(@RequestBody RevisionDTO revisionDTO) {
		 
		 revisionService.save(revisionDTO);
		 
	        return "created/updated revision";
	    }
}
