package com.neo4j.issue.datanodes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Issue : Node will be created. However, relationship to nodes will be missed
 * Verified that for edge from (RevisionNode)-[:HAS_DATA_NODE]-(:DataNode)
 * @author Linda
 *
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
