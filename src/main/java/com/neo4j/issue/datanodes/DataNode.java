package com.neo4j.issue.datanodes;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class DataNode {

	@Id
	@GeneratedValue
	private Long id;
	@Index(unique=true)
	private String uuid;
	private String dataNodeId;
	private String name;
	
	@Relationship(type = "RESIDES_IN" ,direction = Relationship.UNDIRECTED)
	private Set<LoadNode> loadNodes;

	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public String getDataNodeId() {
		return dataNodeId;
	}
	public void setDataNodeId(String dataNodeId) {
		this.dataNodeId = dataNodeId;
	}
	public Set<LoadNode> getLoadNodes() {
		return loadNodes;
	}
	public void setLoadNodes(Set<LoadNode> loadNodes) {
		this.loadNodes = loadNodes;
	}
	
	public void addLoadNodes(Set<LoadNode> loadNodes) {

		if(this.loadNodes == null)
		{
			this.loadNodes = new HashSet<>();
		}
		this.loadNodes.addAll(loadNodes);
	}
}
