package com.neo4j.issue.datanodes;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface RevisionRepository extends Neo4jRepository<RevisionNode,Long>{ 
	
	public RevisionNode findByRevisionNo(int revisionNo);


}
