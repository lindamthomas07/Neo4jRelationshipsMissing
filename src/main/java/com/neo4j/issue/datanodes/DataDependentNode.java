package com.neo4j.issue.datanodes;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class DataDependentNode {

	private Long id;
	
	private String uuid;
	private String name;
	
	@Relationship(type = "DEPENDENT_ON" ,direction = Relationship.UNDIRECTED)
	private Set<DataNode> dataNodes;

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<DataNode> getDataNodes() {
		return dataNodes;
	}

	public void setDataNodes(Set<DataNode> dataNodes) {
		this.dataNodes = dataNodes;
	}

	public Long getId() {
		return id;
	}
	
	
	public void addDataNodes(Set<DataNode> dataNodes) {

		if(this.dataNodes == null)
		{
			this.dataNodes = new HashSet<>();
		}
		this.dataNodes.addAll(dataNodes);
	}
}
