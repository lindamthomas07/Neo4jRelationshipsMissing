package com.neo4j.issue.datanodes;

import org.springframework.data.neo4j.repository.Neo4jRepository;

public interface DataNodeRepository extends Neo4jRepository<DataNode,Long>{ 

	DataNode findByDataNodeId(String dataNodeId);
}
