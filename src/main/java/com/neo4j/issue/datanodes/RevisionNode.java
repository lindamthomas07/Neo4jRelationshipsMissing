package com.neo4j.issue.datanodes;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class RevisionNode {

	@Id
	@GeneratedValue
	private Long id;

	@Index(unique=true)
	private int revisionNo;	
	
	@Relationship(type = "HAS_DATNODE" ,direction = Relationship.UNDIRECTED)
	private Set<DataNode> dataNodes;

	@Relationship(type = "HAS_DEPENDENT" ,direction = Relationship.UNDIRECTED)
	private Set<DataDependentNode> dependentNodes ;

	@Relationship(type = "HAS_LOAD" ,direction = Relationship.UNDIRECTED)
	private Set<LoadNode> loadNodes;

	public Set<DataNode> getDataNodes() {
		return dataNodes;
	}

	public void setDataNodes(Set<DataNode> dataNodes) {
		this.dataNodes = dataNodes;
	}

	public int getRevisionNo() {
		return revisionNo;
	}

	public void setRevisionNo(int revisionNo) {
		this.revisionNo = revisionNo;
	}

	public Long getId() {
		return id;
	}

	public Set<DataDependentNode> getDependentNodes() {
		return dependentNodes;
	}

	public void setDependentNodes(Set<DataDependentNode> dependentNodes) {
		this.dependentNodes = dependentNodes;
	}

	public Set<LoadNode> getLoadNodes() {
		return loadNodes;
	}

	public void setLoadNodes(Set<LoadNode> loadNodes) {
		this.loadNodes = loadNodes;
	}

	public void addDataNodes(Set<DataNode> dataNodes) {

		if(this.dataNodes == null)
		{
			this.dataNodes = new HashSet<>();
		}
		this.dataNodes.addAll(dataNodes);
	}
	
	public void addDependentDataNodes(Set<DataDependentNode> dataDependentNodes) {

		if(this.dependentNodes == null)
		{
			this.dependentNodes = new HashSet<>();
		}
		this.dependentNodes.addAll(dataDependentNodes);
	}
	
	public void addLoadNodes(Set<LoadNode> loadNodes) {

		if(this.loadNodes == null)
		{
			this.loadNodes = new HashSet<>();
		}
		this.loadNodes.addAll(loadNodes);
	}
}
