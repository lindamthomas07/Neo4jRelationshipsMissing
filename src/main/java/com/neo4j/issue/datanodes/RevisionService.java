package com.neo4j.issue.datanodes;

import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RevisionService {


	@Autowired
	private RevisionRepository revisionRepository;

	@Autowired
	private DataNodeRepository dataNodeRepository;

	public void save(RevisionDTO dto){

		RevisionNode revisionNode = revisionRepository.findByRevisionNo(dto.getRevisionNo());
		if(null == revisionNode){
			revisionNode = new RevisionNode();
			revisionNode.setRevisionNo(dto.getRevisionNo());
		}

		/*final RevisionNode revision = revisionNode;
		dto.getDataNodes().forEach((a)->{

			DataNode dataNode = dataNodeRepository.findByDataNodeId(a);
			if(null == dataNode){
				dataNode = new DataNode();
				dataNode.setDataNodeId(a);
				dataNode.setName("DataNode");
				dataNode.setUuid(UUID.randomUUID().toString());
			}			
			revision.addDataNodes(new HashSet<>(Arrays.asList(dataNode)));

		});*/

		for(int i=0;i<28000;i++){
			DataNode dataNode = new DataNode();
			dataNode.setName("DataNode");
			dataNode.setUuid(UUID.randomUUID().toString());			

			DataDependentNode dataDependentNode = new DataDependentNode();
			dataDependentNode.setUuid(UUID.randomUUID().toString());

			LoadNode loadNode = new LoadNode();
			loadNode.setUuid(UUID.randomUUID().toString());

			dataNode.addLoadNodes(new HashSet<>(Arrays.asList(loadNode)));

			dataDependentNode.addDataNodes(new HashSet<>(Arrays.asList(dataNode)));
			
			revisionNode.addDependentDataNodes(new HashSet<>(Arrays.asList(dataDependentNode)));
			revisionNode.addDataNodes(new HashSet<>(Arrays.asList(dataNode)));
			revisionNode.addLoadNodes(new HashSet<>(Arrays.asList(loadNode)));
		}

		revisionRepository.save(revisionNode);

	}
}
