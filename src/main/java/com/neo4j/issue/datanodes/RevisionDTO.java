package com.neo4j.issue.datanodes;

import java.util.List;

public class RevisionDTO {

	private int revisionNo;
	private List<String> dataNodes;
	
	public int getRevisionNo() {
		return revisionNo;
	}
	public void setRevisionNo(int revisionNo) {
		this.revisionNo = revisionNo;
	}
	public List<String> getDataNodes() {
		return dataNodes;
	}
	public void setDataNodes(List<String> dataNodes) {
		this.dataNodes = dataNodes;
	}
}
